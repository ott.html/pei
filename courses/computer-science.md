# Computer Science

## Subject requirements:

1.  [CS01-Computation Structures](../subjects/computation-structures/computation-structures.md)
2.  [CS02-Introduction to Algorithms](../subjects/introduction-to-algorithms.md)
3.  [CS03-Fundamentals of Programming](../subjects/fundamentals-of-programming.md)
4.  [CS04-Elements of Software Construction](../subjects/elements-of-software-construction.md)
5.  [CS05-Computer Systems Engineering](../subjects/computer-systems-engineering.md)
6.  [CS06-Computability and Complexity Theory](../subjects/computability-and-complexity-theory.md)
7.  [CS07-Design and Analysis of Algorithms](../subjects/design-and-analysis-of-algorithms.md)
8.  [CS08-Software Performance Engineering](../subjects/software-performance-engineering.md)
9.  [CS09-Theory of Computation](../subjects/theory-of-computation.md)
10. [CS10-Introduction to Machine Learning](../subjects/introduction-to-machine-learning.md)